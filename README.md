# Aptgetoide

This project is a python web based app that allows to gather information about an Aptoid given its URL.

# Directories

* web: Front end files
  * js: javascript files
* python2: files for python2's version
* python3: files for python3's version

# Start

Start httpServer.py from either python2 or python3's directories.

# Test

Apart from 'httpServer.py's' files, all files can be launched separately for unit testing purposes.
