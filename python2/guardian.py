# -*- coding: utf-8 -*-
"""
The guardian handles the inputs' validity.

It mostly consists of assertions and exception raising.
"""

import unittest
import re

def assert_consistent_url(url):
  """
  Assert that the address is a valid URL
  i.e.
  - Starts with "https://"
  - Ends with ".en.aptoide.com" (plus / ?)
  """
  assert re.match('\Ahttps://[^.]*?\.[a-zA-Z]{2}\.aptoide\.com/?\Z', url)
  return True







class GuardianTest(unittest.TestCase):
  
  def test_assert_consistent_url(self):
    failTests = [
      'http://test.en.aptoide.com',
      'https://test.en.aptoid.com',
      'https://test.en.aptoide.net',
      'https://test.aptoide.com',
      'https://test.en.aptoide.comtest',
      'testhttps://test.en.aptoide.com',
      'https://test.en.aptoide.com/?test=test.en.aptoide.com'
    ]
    for test in failTests:
      with self.assertRaises(AssertionError):
        assert_consistent_url(test)
    
    successTests = [
      'https://test.en.aptoide.com/',
      'https://test.en.aptoide.com',
      'https://test-test.en.aptoide.com/',
      'https://test-test.fr.aptoide.com/',
      'https://test-test.FR.aptoide.com/',
      'https://test-test.Fr.aptoide.com/'
    ]
    for test in successTests:
      self.assertTrue(assert_consistent_url(test))
    

if __name__ == '__main__':
  unittest.main()

