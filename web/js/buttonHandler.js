$(function() {
    $('#getdatabutton').on('click', function (e) {e.preventDefault();
		$.ajax({
		    type: "GET",
		    url: "/index.html",
		    data: { 
		        apturl: 'https://'+$("#apturl").val()+'.en.aptoide.com'
		    },
		    success: function(result) {
		    	if(result['status'] == 'valid'){
		    		$('#appNameAndVersion').html(result['name']+' ('+result['version']+')');
		    		$('#releaseDate').html(result['releaseDate']);
		    		$('#appDesc').html(result['description']);
		    		$('#downloads').html('Downloaded '+result['downloads']+' times.');
		    	}else{
		    		$('#appNameAndVersion').html('None');
		    		$('#releaseDate').html('Might release someday...');
		    		$('#appDesc').html('The application you requested does not exist (yet).');
		    		$('#downloads').html('Has never been downloaded yet.');
		    	}
		    },
		    error: function(result) {
		        alert('error');
		    }
		});
    });
});
