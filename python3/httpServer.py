# -*- coding: utf-8 -*-
"""
The httpServer will handle every requests from the server.
"""

from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs
import re
import json
import getter
import guardian

PORT = 8080

class HttpServer(BaseHTTPRequestHandler):
  def do_GET(self):
    page_dir = '../web/'  
    try:
      if self.path.endswith('.html') or self.path.endswith('.js') or self.path.endswith('.css'):
        print('Enter with path: ', self.path)
        f = open(page_dir + self.path)
        self.send_response(200)
        self.send_header('Content-type','text-html')
        self.end_headers()
        self.wfile.write(bytes(f.read(), "utf-8"))
        f.close()  
        return
      else:
        self.send_response(200)
        self.send_header('Content-type','text-json')
        self.end_headers()
        try:
          apturl = parse_qs(urlparse(self.path).query)['apturl'][0]
        except KeyError:
          print(self.path)
          self.send_error(500, 'Internal error')
          return
        print(apturl)
        guardian.assert_consistent_url(apturl)
        m = re.search('\Ahttps://([^.]*?)\.[a-zA-Z]{2}\.aptoide\.com/?\Z', apturl)
        if m:
          uname = m.group(1)
          data = getter.get_package_data(uname)
          print(data)
          if data == 'invalid':
            self.wfile.write(bytes(json.dumps({'status':'invalid'}), "utf-8"))
          else:
            data['status'] = 'valid'
            self.wfile.write(bytes(json.dumps(data), "utf-8"))
        else:
          self.wfile.write(bytes(json.dumps({'status':'invalid'}), "utf-8"))
        return
    except IOError as error:  
      self.send_error(404, 'File not found.')
    except:
      self.send_error(500, 'Internal error')

try:
  PORT = 8080
  server = HTTPServer(('', PORT), HttpServer)
  print('Started HttpServer at port ', PORT)
  # Awaits requests
  server.serve_forever()
except KeyboardInterrupt:
	print("Server is shutting down on user's demand.")
	server.socket.close()
	
