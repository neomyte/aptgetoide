# -*- coding: utf-8 -*-
"""
The getter handles the get requests.
"""

import unittest
import urllib.request
import json

def get_package_data(package_uname):
  try:
  	answer = urllib.request.urlopen("https://ws75.aptoide.com/api/7/app/get/package_uname="+package_uname).read()
  except:
    return 'invalid'
  content = json.loads(answer)
  try:
    assert content['info']['status'] == 'OK'
    assert content['nodes']['meta']['info']['status'] == 'OK'
  except AssertionError as error:
    return 'invalid'
  content = content['nodes']['meta']['data']    
  return {
  	"name": content['name'],
  	"version": content['file']['vername'],
  	"downloads": content['stats']['downloads'],
  	"releaseDate": content['added'].split(' ')[0],
  	"description": content['media']['description']
  }

class GuardianTest(unittest.TestCase):
  
  def test_get_package_data(self):
    valid_app_name = 'era-of-celestials'
    data = get_package_data(valid_app_name)
    # assert fields are set
    for key in  ["name", "version", "downloads", "releaseDate", "description"]:
      self.assertTrue(key in data)
    
    # assert validity of field (when possible)
    self.assertTrue(data["name"] == "Era of Celestials")
    self.assertTrue(data["releaseDate"] == "2018-11-05")
    
if __name__ == '__main__':
  unittest.main()
